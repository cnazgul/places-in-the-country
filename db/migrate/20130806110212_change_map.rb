class ChangeMap < ActiveRecord::Migration
  def up
    add_column :maps, :more, :text
    remove_column :maps, :img_url
  end

  def down
    remove_column :maps, :more
    add_column :maps, :img_url, :string
  end
end
