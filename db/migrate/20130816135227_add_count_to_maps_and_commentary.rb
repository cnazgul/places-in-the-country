class AddCountToMapsAndCommentary < ActiveRecord::Migration
  def up
    add_column :maps, :moderation, :integer, :default => 1
    add_column :commentaries, :moderation, :integer, :default => 1
  end

  def down
    remove_column :maps, :moderation
    remove_column :commentaries
  end

end
