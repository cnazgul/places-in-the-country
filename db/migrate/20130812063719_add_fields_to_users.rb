class AddFieldsToUsers < ActiveRecord::Migration
  def up
    add_column :users, :avatar, :string
    add_column :users, :sex, :integer
    add_column :users, :mailing, :boolean
    add_column :users, :age, :integer
    add_column :users, :about, :string
  end

  def down
    remove_column :users, :avatar
    remove_column :users, :sex
    remove_column :users, :mailing
    remove_column :users, :age
    remove_column :users, :about
  end
end
