class RemoveColumnToUsers < ActiveRecord::Migration
  def up
    remove_column :users, :sex
    remove_column :users, :sex2
    add_column :users, :sex, :string
  end

  def down
    add_column :users, :sex, :integer
    add_column :users, :sex2, :string
    remove_column :users, :sex
  end
end
