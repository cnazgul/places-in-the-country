class UpdateColumnCoordToMaps < ActiveRecord::Migration
  def up
    change_table :maps do |t|
      t.change :coord, :string
    end
  end

  def down
    change_table :maps do |t|
      t.change :coord, :float
    end
  end
end
