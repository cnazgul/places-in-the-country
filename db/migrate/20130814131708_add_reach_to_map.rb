class AddReachToMap < ActiveRecord::Migration
  def up
    add_column :maps, :reach, :string
  end

  def down
    remove_column :maps, :reach
  end
end
