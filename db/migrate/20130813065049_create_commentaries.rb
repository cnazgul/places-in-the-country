class CreateCommentaries < ActiveRecord::Migration
  def change
    create_table :commentaries do |t|
      t.integer :user_id
      t.string :description
      t.integer :map_id

      t.timestamps
    end
  end
end
