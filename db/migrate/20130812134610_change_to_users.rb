class ChangeToUsers < ActiveRecord::Migration
  def up
    remove_column :users, :sex
    add_column :users, :sex, :integer
  end

  def down
    remove_column :users, :sex
    add_column :users, :sex, :string
  end
end
