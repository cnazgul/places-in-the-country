class AddSex2ToUsers < ActiveRecord::Migration
  def up
    add_column :users, :sex2, :string
  end

  def down
    remove_column :users, :sex2
  end
end
