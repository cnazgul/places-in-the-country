class AddFieldsForMap < ActiveRecord::Migration
  def up
    add_column :maps, :category_id, :integer
    add_column :maps, :title, :string
    add_column :maps, :img_url, :string
    add_column :maps, :coord, :float
    add_column :maps, :rating, :float
  end

  def down
    remove_column :maps, :category_id, :integer
    remove_column :maps, :title, :string
    remove_column :maps, :img_url, :string
    remove_column :maps, :coord, :float
    remove_column :maps, :rating, :float
  end
end
