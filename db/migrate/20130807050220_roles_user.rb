class RolesUser < ActiveRecord::Migration
  def up
    add_column :roles, :name, :string
    create_table :roles_users, :id => false do |t|
      t.references :role, :user
    end
    drop_table :users_roles

  end

  def down
    drop_table :roles_users
    remove_column :roles, :name

  end
end
