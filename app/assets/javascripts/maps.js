//# Place all the behaviors and hooks related to the matching controller here.
//# All this logic will automatically be available in application.js.
//# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

// Как только будет загружен API и готов DOM, выполняем инициализацию
ymaps.ready(init);

function init () {

// блок работы с картой
// Создание экземпляра карты и его привязка к контейнеру с
// заданным id ("map")
    var myMap = new ymaps.Map('map', {
        center: [53.21, 50.10], // samara
        zoom: 12
    }),
    searchControl = new SearchAddress(myMap, $('form.test'));

// шаблон балуна
    BalloonContentLayout = ymaps.templateLayoutFactory.createClass (
        '<div style="margin:0px; width: 310px; height: 150px;">' +
            '<div style="position:absolute; top:5px; left:10px; width:140px; height:100px; text-align:center;">' +
            '<div style="position:absolute; top: 0px; width:120px; text-align:center; color:#5f4e4e;  font-weight:bold;">' +
            '$[properties.balloonContent]' +
            '</div>' +
            '<div style="position:absolute; top: 55px">' +
            '<div class="balloon-btn">' +
            '<div  class="balloon-btn-text"><span><a id="more" href="/?point=' +
            '$[properties.id]' +
            '" data-remote="true">' +
            'Подробнее</a></span></div> ' +
            '</div>' +
            '</div>' +
            '<div style="position:absolute; top: 85px">' +
            '<div class="balloon-btn">' +
            '<div  class="balloon-btn-text"><span><a id="howtoreach" href="/?point=' +
            '$[properties.id]' +
            '" data-remote="true">' +
            'Как доехать?</a></span></div> ' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div style="position:absolute; top:10px; left:150px; width:160px;">' +
            '<img style="width:170px; height:110px;" src="$[properties.urlImg]">' +
            '</div>' +
            '<div style="position:absolute;top:125px; height: 35px; left:10px; width:300px; text-align:center;">' +
            '<div style="left: 40px;" class="rating" data="7_3"></div>' +
            '<span>' +
            '<a id="comment" href="/?point=' +
            '$[properties.id]' +
            '" data-remote="true">' +
//            '<a href="/" data-remote="true" id="20">' +
            'Комментарии </a>' +
/*
            '<script type="text/javascript">' +
            '$("img").click(function() { ' +
            'alert ("dg");' +
            '$("fieldset#add_comment").toggle();' +
            '});' +
            '</script>' +
*/
            '<img class="comment" id="$[properties.id]" style="width:12px; cursor:pointer;" src="/assets/add.png">' +
//            '<a href="#"><img style="width:12px;" src="/assets/add.png"></a>' +
//            '<br>' +
//            '= link_to "fg", new_commentary' +
            '</span>' +
            '</div>' +
            '</div>', {

                // Переопределяем функцию build, чтобы при создании макета начинать
                // слушать событие click на кнопке-счетчике.
                build: function () {
                    // Сначала вызываем метод build родительского класса.
                    BalloonContentLayout.superclass.build.call(this);
                    // А затем выполняем дополнительные действия.
// форма добавления комментария
                    $('img.comment').bind('click', this.onAddCommentClick);
                    $('#comment').bind('click', this.onFormCommentClick);
                    $('#howtoreach').bind('click', this.onFormHowToReachClick);
                    $('#more').bind('click', this.onFormMoreClick);
                },

                // Аналогично переопределяем функцию clear, чтобы снять
                // прослушивание клика при удалении макета с карты.
                clear: function () {
                    // Выполняем действия в обратном порядке - сначала снимаем слушателя,
                    // а потом вызываем метод clear родительского класса.
                    $('img.comment').unbind('click', this.onAddCommentClick);
                    $('#comment').unbind('click', this.onFormCommentClick);
                    $('#howtoreach').unbind('click', this.onFormHowToReachClick);
                    $('#more').unbind('click', this.onFormMoreClick);
                    BalloonContentLayout.superclass.clear.call(this);
                },

                onFormCommentClick: function () {
                    $('fieldset#form_comment').toggle();
//                    map_id = $("span.comment").attr('id');
//                    $("input#commentary_map_id").val(map_id);
                    $('fieldset#form_comment').mouseup(function() {
                        return false
                    });
                    $(document).mouseup(function(e) {
                        $('fieldset#form_comment').hide();
                    });
                },

                onAddCommentClick: function () {
                    $('fieldset#add_comment').toggle();
                    map_id = $("img.comment").attr('id');
                    $("input#commentary_map_id").val(map_id);
                    $('fieldset#add_comment').mouseup(function() {
                        return false
                    });
                    $(document).mouseup(function(e) {
                        $('fieldset#add_comment').hide();
                    });
                },

                onFormHowToReachClick: function () {
                    $('fieldset#form_howtoreach').toggle();
//                    map_id = $("img.comment").attr('id');
//                    $("input#commentary_map_id").val(map_id);
                    $('fieldset#form_howtoreach').mouseup(function() {
                        return false
                    });
                    $(document).mouseup(function(e) {
                        $('fieldset#form_howtoreach').hide();
                    });
                },

                onFormMoreClick: function () {
                    $('fieldset#form_more').toggle();
    //                    map_id = $("img.comment").attr('id');
    //                    $("input#commentary_map_id").val(map_id);
                    $('fieldset#form_more').mouseup(function() {
                        return false
                    });
                    $(document).mouseup(function(e) {
                        $('fieldset#form_more').hide();
                    });
                },
            }

    );
    ymaps.layout.storage.add('my#simplestBCLayout', BalloonContentLayout);


// коллекция достопримечательности
// id - id object img in gallery in the view
    var j = 0;
    myDostopCollection = new ymaps.GeoObjectCollection({}, {
      preset: 'twirl#buildingsIcon',
    });
    for (var i = 0; i < gon.map_title[0].length; i++) {
      myDostopCollection.add(new ymaps.Placemark([gon.map_coord[0][j],gon.map_coord[0][j+1]], {
        balloonContent: gon.map_title[0][i],
        urlImg: gon.map_img[0][i],
        id: gon.map_id[0][i],
      }));
      j = j + 2;
    }
// Подключаем шаблон балуна к коллекции
    myDostopCollection.options.set({
      balloonContentBodyLayout:'my#simplestBCLayout',
    });
    myMap.geoObjects.add(myDostopCollection);


// коллекция интересные места
    j = 0;
    myIntCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'twirl#photographerIcon',
    });
    for (var i = 0; i < gon.map_title[1].length; i++) {
        myIntCollection.add(new ymaps.Placemark([gon.map_coord[1][j],gon.map_coord[1][j+1]], {
            balloonContent: gon.map_title[1][i],
            urlImg: gon.map_img[1][i],
            id: gon.map_id[1][i],
        }));
        j = j + 2;
    }
// Подключаем шаблон балуна к коллекции
    myIntCollection.options.set({
        balloonContentBodyLayout:'my#simplestBCLayout'
    });
    myMap.geoObjects.add(myIntCollection);


// коллекция арт
    j = 0;
    myArtCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'twirl#cafeIcon',
    });
    for (var i = 0; i < gon.map_title[3].length; i++) {
        myArtCollection.add(new ymaps.Placemark([gon.map_coord[3][j],gon.map_coord[3][j+1]], {
            balloonContent: gon.map_title[3][i],
            urlImg: gon.map_img[3][i],
            id: gon.map_id[3][i],
        }));
        j = j + 2;
    }
// Подключаем шаблон балуна к коллекции
    myArtCollection.options.set({
        balloonContentBodyLayout:'my#simplestBCLayout'
    });
    myMap.geoObjects.add(myArtCollection);


// коллекция хочу больше!
    j = 0;
    myBigCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'twirl#tireIcon',
    });
    for (var i = 0; i < gon.map_title[5].length; i++) {
        myBigCollection.add(new ymaps.Placemark([gon.map_coord[5][j],gon.map_coord[5][j+1]], {
            balloonContent: gon.map_title[5][i],
            urlImg: gon.map_img[5][i],
            id: gon.map_id[5][i],
        }));
        j = j + 2;
    }
// Подключаем шаблон балуна к коллекции
    myBigCollection.options.set({
        balloonContentBodyLayout:'my#simplestBCLayout'
    });
    myMap.geoObjects.add(myBigCollection);


// коллекция Встречи
    j = 0;
    myMeetCollection = new ymaps.GeoObjectCollection({}, {
        preset: 'twirl#bowlingIcon',
    });
    for (var i = 0; i < gon.map_title[4].length; i++) {
        myMeetCollection.add(new ymaps.Placemark([gon.map_coord[4][j],gon.map_coord[4][j+1]], {
            balloonContent: gon.map_title[4][i],
            urlImg: gon.map_img[4][i],
            id: gon.map_id[4][i],
        }));
        j = j + 2;
    }
// Подключаем шаблон балуна к коллекции
    myMeetCollection.options.set({
        balloonContentBodyLayout:'my#simplestBCLayout'
    });
    myMap.geoObjects.add(myMeetCollection);


// коллекция Ожидаемое
    /*
     coord_ = [],
     uid = [],
     place_ = [];
     url = [],
     myExpecCollection = new ymaps.GeoObjectCollection({}, {
     preset: 'twirl#tireIcon',
     });
     for (var i = 0; i < coord_.length; i++) {
     my_Collection.add(new ymaps.Placemark(coord_[i], {
     balloonContent: place_[i],
     urlImg: url[i],
     id: uid[i],
     }));
     }
     // Подключаем шаблон балуна к коллекции
     myExpecCollection.options.set({
     balloonContentBodyLayout:'my#simplestBCLayout',
     });
     myMap.geoObjects.add(my_Collection);
     */

// функции на карте
    myMap.controls
        .add('trafficControl')
        .add('zoomControl', {right : '35px'})
        .add('mapTools', {right : '250px'});

// включаем scroll Zoom
    myMap.behaviors
        .enable('scrollZoom');

// форма входа
    $(".sign").click(function() {
        $("fieldset#signin_menu").toggle();
    });

    $("fieldset#signin_menu").mouseup(function() {
        return false
    });
    $(document).mouseup(function(e) {
        $("fieldset#signin_menu").hide();
    });

// форма регистрация
    $("#reg_button").click(function() {
        $("fieldset#reg_menu").toggle();
        $("fieldset#signin_menu").hide();
    });

    $("fieldset#reg_menu").mouseup(function() {
        return false
    });
    $(document).mouseup(function(e) {
        $("fieldset#reg_menu").hide();
    });

// форма редактирование профиля
     $('div#profile').click(function() {
     $('fieldset#prof_menu').toggle();
     });

     $('fieldset#prof_menu').mouseup(function() {
     return false
     });
     $(document).mouseup(function(e) {
     $('fieldset#prof_menu').hide();
     });

// плагин скролбара
    $(".scroll-pane").jScrollPane({showArrows:false});

// поведение галереи сайта при клике по рабочей области
    $(".layer2, .gal a img").click(function() {
        $(".layer1").css("width","20%");
        $(".layer1").css("min-width","150px");
        $(".layer3").css("visibility","visible");

        $(".hello").remove();

        $(".gal img").css("width","80%");
        $(".gal img").css("margin","10%");
        $(".gal").css("top","1%");

    });

// поведение чекбоксов фильтров
    $("div.chkDiv").addClass('checkOn');
    $('.chkDiv input:checkbox').attr("checked",true);

    /*далее костыль: --- в рельсах, кстати, работает как надо, т.е. без костыля
     Как ведется себя jquery (м.б. и нативный js)
     При клике на чекбоксе мышью:
     1) переключить checked;
     2) выполнить повешенные на клик обработчики.

     При программном вызове click():
     1) выполнить повешенные на клик обработчики;
     2) переключить checked.
     т.е. логика if () else выходит обратной относительно значения attr('checked')
     */
    $('.chkDiv input[type=checkbox]').click(function() {
        if ($(this).attr('checked')) {
//            $(this).attr("checked",false); // для рельс, жестко переключаем чекбокс
            $($(this).parent()[0]).removeClass("checkOff");
            $($(this).parent()[0]).addClass("checkOn");

            switch ($(this).attr('id')) {
                case 'myDostopCollection':
                    myMap.geoObjects.add(myDostopCollection);
                    $('.myDostopCollection').show();
                    break
                case 'myIntCollection':
                    myMap.geoObjects.add(myIntCollection);
                    $('.myIntCollection').show();
                    break
                case 'myArtCollection':
                    myMap.geoObjects.add(myArtCollection);
                    $('.myArtCollection').show();
                    break
                case 'myBigCollection':
                    myMap.geoObjects.add(myBigCollection);
                    $('.myBigCollection').show();
                    break
                case 'myMeetCollection':
                    myMap.geoObjects.add(myMeetCollection);
                    $('.myMeetCollection').show();
                    break
                /*
                 case '':
                 myMap.geoObjects.add();
                 $('.').show();
                 break
                 */
            }
        } else {
//            $(this).attr("checked",true)   // для рельс, жестко переключаем чекбокс
            $($(this).parent()[0]).removeClass("checkOn");
            $($(this).parent()[0]).addClass("checkOff");

            switch ($(this).attr('id')) {
                case 'myDostopCollection':
                    myMap.geoObjects.remove(myDostopCollection);
                    $('.myDostopCollection').hide();
                    break
                case 'myIntCollection':
                    myMap.geoObjects.remove(myIntCollection);
                    $('.myIntCollection').hide();
                    break
                case 'myArtCollection':
                    myMap.geoObjects.remove(myArtCollection);
                    $('.myArtCollection').hide();
                    break
                case 'myBigCollection':
                    myMap.geoObjects.remove(myBigCollection);
                    $('.myBigCollection').hide();
                    break
                case 'myMeetCollection':
                    myMap.geoObjects.remove(myMeetCollection);
                    $('.myMeetCollection').hide();
                    break
                /*
                 case '':
                 myMap.geoObjects.remove();
                 $('.').hide();
                 break
                 */
            }
        }
    });

// Вкл/Выкл всех фильтров
    $('.checkall').click(function(){
        $('div.chkDiv').removeClass("checkOff")
            .addClass("checkOn");
        $('input:checkbox').attr("checked",true);
        myMap.geoObjects
            .add(myDostopCollection)
            .add(myIntCollection)
            .add(myArtCollection)
            .add(myBigCollection)
            .add(myMeetCollection);
        $('.myDostopCollection, .myIntCollection, .myArtCollection, .myBigCollection, .myMeetCollection').show();
    });
    $('.uncheckall').click(function(){
        $('div.chkDiv').removeClass("checkOn")
            .addClass("checkOff");
        $('input:checkbox').attr("checked",false);
        myMap.geoObjects
            .remove(myDostopCollection)
            .remove(myIntCollection)
            .remove(myArtCollection)
            .remove(myBigCollection)
            .remove(myMeetCollection);
        $('.myDostopCollection, .myIntCollection, .myArtCollection, .myBigCollection, .myMeetCollection').hide();
    });

// открываем балун при клике на соответствующее фото в галерее
    $(".gal a img").click (function(){
        switch ($(this).attr('class')) {
            case 'myDostopCollection':
                var imgid = $(this).attr('id');
                myDostopCollection.each(function (item) {
                    if (item.properties.get('id') == imgid) {
                        item.balloon.open();
                        myMap.setCenter(item.geometry.getCoordinates());
                    }
                });
                break
            case 'myIntCollection':
                var imgid = $(this).attr('id');
                myIntCollection.each(function (item) {
                    if (item.properties.get('id') == imgid) {
                        item.balloon.open();
                        myMap.setCenter(item.geometry.getCoordinates());
                    }
                });
                break
            case 'myArtCollection':
                var imgid = $(this).attr('id');
                myArtCollection.each(function (item) {
                    if (item.properties.get('id') == imgid) {
                        item.balloon.open();
                        myMap.setCenter(item.geometry.getCoordinates());
                    }
                });
                break
            case 'myBigCollection':
                var imgid = $(this).attr('id');
                myBigCollection.each(function (item) {
                    if (item.properties.get('id') == imgid) {
                        item.balloon.open();
                        myMap.setCenter(item.geometry.getCoordinates());
                    }
                });
                break
            case 'myMeetCollection':
                var imgid = $(this).attr('id');
                myMeetCollection.each(function (item) {
                    if (item.properties.get('id') == imgid) {
                        item.balloon.open();
                        myMap.setCenter(item.geometry.getCoordinates());
                    }
                });
                break
            /*
             case 'myIntCollection':
             myMap.geoObjects.remove(myIntCollection);
             break
             */
        }
    });

// подсвечиваем метку при наведении на соответствующее фото в галерее
    $(".gal a img").hover (function(){
            switch ($(this).attr('class')) {
                case 'myDostopCollection':
                    var imgid = $(this).attr('id');
                    myDostopCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#violetDotIcon');
                        }
                    });
                    break
                case 'myIntCollection':
                    var imgid = $(this).attr('id');
                    myIntCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#blackDotIcon');
                        }
                    });
                    break
                case 'myArtCollection':
                    var imgid = $(this).attr('id');
                    myArtCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#brownDotIcon');
                        }
                    });
                    break
                case 'myBigCollection':
                    var imgid = $(this).attr('id');
                    myBigCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#nightDotIcon');
                        }
                    });
                    break
                case 'myMeetCollection':
                    var imgid = $(this).attr('id');
                    myMeetCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#greenDotIcon');
                        }
                    });
                    break
                /*
                 case '':
                 myMap.geoObjects.remove();
                 break
                 */
            }
        },
        function(){
            switch ($(this).attr('class')) {
                case 'myDostopCollection':
                    var imgid = $(this).attr('id');
                    myDostopCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#buildingsIcon');
                        }
                    });
                    break
                case 'myIntCollection':
                    var imgid = $(this).attr('id');
                    myIntCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#photographerIcon');
                        }
                    });
                    break
                case 'myArtCollection':
                    var imgid = $(this).attr('id');
                    myArtCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#cafeIcon');
                        }
                    });
                    break
                case 'myBigCollection':
                    var imgid = $(this).attr('id');
                    myBigCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#tireIcon');
                        }
                    });
                    break
                case 'myMeetCollection':
                    var imgid = $(this).attr('id');
                    myMeetCollection.each(function (item) {
                        if (item.properties.get('id') == imgid) {
                            item.options.set('preset','twirl#bowlingIcon');
                        }
                    });
                    break
                /*
                 case '':
                 myMap.geoObjects.remove();
                 break
                 */
            }
        });

// -------------------------------
     var btn_check = true;
     var onClick;
    $(".metka-btn").click(function() {
      if (btn_check) {
        $(".metka-btn").css("font-weight","bold");
        onClick = function (e) {
          // Географические координаты точки клика можно узнать
          // посредством вызова .get('coordPosition')
          var position = e.get('coordPosition');
          $("input#map_coord").val(position);
          // форма добавления метки
          $("fieldset#add_point").toggle();

          btn_check = false;

          // скрываем поле для ввода метки
          $("#point").click(function() {
            $("fieldset#add_point").hide();
          });

        };
        myMap.events.add('click', onClick)
        } else {
          myMap.events.remove('click', onClick);
          $(".metka-btn").css("font-weight","normal");
          btn_check = true;
        }

    });

// добавляем рейтинг в тело баллуна
    myMap.balloon.events.add('open', function () {
        $('.rating').jRating({
            step:true,
            length : 10
        });
    });

}