class CategoriesController < ApplicationController

  load_and_authorize_resource
  before_filter :find_cat, :only => [:destroy, :update, :edit, :show]

  # GET /categories
  # GET /categories.json
  def index
    @maps = Map.select("id, title, category_id, user_id, image")
    @categories = Category.select("id, title")
    @roles = Role.select("id, name")
    @users = User.select("ID,Name,Email")

    @maps_mod = Map.moderation(1).select("id, created_at, title, user_id")
    @comment_mod = Commentary.moderation(1)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /categories/1
  # GET /categories/1.json
  def show

    respond_to do |format|
      format.html # _show.html.slim
    end
  end

  # GET /categories/new
  # GET /categories/new.json
  def new
    @category = Category.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /categories/1/edit
  def edit

  end

  # POST /categories
  # POST /categories.json
  def create
    @category = Category.new(params[:category])

    respond_to do |format|
      if @category.save
        format.html { redirect_to @category, notice: 'Category was successfully created.' }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.json
  def update

    respond_to do |format|
      if @category.update_attributes(params[:category])
        format.html { redirect_to @category, notice: 'Category was successfully updated.' }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.json
  def destroy
    @category.destroy

    respond_to do |format|
      format.html { redirect_to categories_url }
    end
  end

  def moderation_comment
    @comm = Commentary.find(params[:id])

    respond_to do |format|
      @comm.update_attribute(:moderation, 0)
      format.js
    end
  end

  def moderation_map
    @map = Map.find(params[:id])

    respond_to do |format|
      @map.update_attribute(:moderation, 0)
      format.js
    end
  end

  private
  def find_cat
    @category = Category.find(params[:id].to_i)
  end

end

