class UsersController < ApplicationController

  load_and_authorize_resource
  before_filter :find_user, :only => [:destroy, :update, :edit, :show]

    def index

      respond_to do |format|
        format.html # index.html.erb
      end
    end

    def show

      respond_to do |format|
        #format.html # _show.html.slim
        format.js
      end
    end

    def new
      @category = Category.new

      respond_to do |format|
        format.html # new.html.erb
      end
    end

    def edit

    end

    def create
      @category = Category.create(params[:category])

      respond_to do |format|
        if @category.save
          format.html { redirect_to @category, notice: 'Category was successfully created.' }
        else
          format.html { render action: "new" }
        end
      end
    end

    def update

      respond_to do |format|
        if @user.update_attributes(params[:user])
          format.html { redirect_to root_path }
        else
          format.html { render action: "edit" }
        end
      end
    end

    def destroy
      @user.destroy

      respond_to do |format|
        format.html { redirect_to categories_url }
      end
    end

    private
    def find_user
      @user = User.find(params[:id].to_i)
    end

end
