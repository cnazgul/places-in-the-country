class MapsController < ApplicationController

  load_and_authorize_resource :except => [:index, :show]
  before_filter :find_map, :only => [:destroy, :update, :edit, :show]

  # GET /maps
  # GET /maps.json
  def index
    @maps = Map.moderation(0)

    if params[:point]
      point = params[:point].to_i
      @point = Map.find(point)
      @commentaries = Commentary.find_by_map_id(point)
      @user = User.find_by_id(params[:user].to_i)
    else
      @map = Map.new
      Map.unload_place(gon)
      user_present?()
    end

    respond_to do |format|
      format.html
      format.js
    end
  end

  # GET /maps/1
  # GET /maps/1.json
  def show

    respond_to do |format|
      format.html # _show.html.slim
    end
  end

  # GET /maps/new
  # GET /maps/new.json
  def new
    @map = Map.new

    respond_to do |format|
      format.html # new.html.slim
    end
  end

  # GET /maps/1/edit
  def edit

  end

  # POST /maps
  # POST /maps.json
  def create
    @map = current_user.maps.new(params[:map])

    respond_to do |format|
      if @map.save
        format.html { redirect_to root_path }
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /maps/1
  # PUT /maps/1.json
  def update

    respond_to do |format|
      if @map.update_attributes(params[:map])
        format.html { redirect_to categories_path }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /maps/1
  # DELETE /maps/1.json
  def destroy
    @map.destroy

    respond_to do |format|
      format.html { redirect_to maps_url }
    end
  end

  private
  def user_present?
    if current_user.present?
      @user = User.find(current_user)
      @commentary = current_user.commentaries.new
      @count_map = Map.moderation(1).count
      @count_commentary = Commentary.moderation(1).count
    end
  end

  def find_map
    @map = Map.find(params[:id].to_i)
  end

end
