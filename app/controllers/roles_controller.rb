class RolesController < ApplicationController

  load_and_authorize_resource
  before_filter :find_role, :only => [:destroy, :update, :edit, :show]

    def index

      respond_to do |format|
        format.html # index.html.erb
      end
    end

    def show

      respond_to do |format|
        format.html # _show.html.slim
      end
    end

    def new
      @role = Role.new

      respond_to do |format|
        format.html # new.html.erb
      end
    end

    def edit

    end

    def create
      @role = Role.create(params[:role])

      respond_to do |format|
        if @role.save
          format.html { redirect_to categories_url }
        else
          format.html { render action: "new" }
        end
      end
    end

    def update

      respond_to do |format|
        if @role.update_attributes(params[:role])
          format.html { redirect_to categories_url }
        else
          format.html { render action: "edit" }
        end
      end
    end

    def destroy
      @role.destroy

      respond_to do |format|
        format.html { redirect_to categories_url }
      end
    end

    private
    def find_role
      @role = Role.find(params[:id].to_i)
    end

end
