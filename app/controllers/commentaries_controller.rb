class CommentariesController < ApplicationController

  load_and_authorize_resource :except => [:index, :show]
  before_filter :find_comment, :only => [:destroy, :update, :edit, :show]

  # GET /commentaries
  # GET /commentaries.json
  def index
    @commentaries = Commentary.find_by_map_id(params[:id].to_i)

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /commentaries/1
  # GET /commentaries/1.json
  def show

    respond_to do |format|
      format.html # _show.html.slim
    end
  end

  # GET /commentaries/new
  # GET /commentaries/new.json
  def new
    @commentary = Commentary.new

    respond_to do |format|
      format.html # new.html.erb
    end
  end

  # GET /commentaries/1/edit
  def edit

  end

  # POST /commentaries
  # POST /commentaries.json
  def create
    @commentaries = Commentary.find_by_map_id(params[:commentary][:map_id].to_i)
    @commentary = current_user.commentaries.new(params[:commentary])

    respond_to do |format|
      if @commentary.save
        format.html { redirect_to @commentary, notice: 'Commentary was successfully created.' }
        format.js {}
      else
        format.html { render action: "new" }
      end
    end
  end

  # PUT /commentaries/1
  # PUT /commentaries/1.json
  def update

    respond_to do |format|
      if @commentary.update_attributes(params[:commentary])
        format.html { redirect_to root_path }
      else
        format.html { render action: "edit" }
      end
    end
  end

  # DELETE /commentaries/1
  # DELETE /commentaries/1.json
  def destroy
    @commentary.destroy

    respond_to do |format|
      format.js
    end
  end

  private
  def find_comment
    @commentary = Commentary.find(params[:id].to_i)
  end

end