module MapsHelper

  def resource_name
    :user
  end

  def resource
    @resource ||= User.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def img_class(category_id)
    case category_id
      when 1
        @img_class = "myDostopCollection"
      when 2
        @img_class = "myIntCollection"
      when 3
        @img_class = ""
      when 4
        @img_class = "myArtCollection"
      when 5
        @img_class = "myMeetCollection"
      when 6
        @img_class = "myBigCollection"
    end
  end

  def avatar?(o,w,h)
    if o.avatar?
      raw "<img src=\"#{o.avatar}\" width=\"#{w}\" height=\"#{h}\" alt=\"Avatar\">"
    else
      raw "<img src=\"/assets/noavatar.png\" width=\"#{w}\" height=\"#{h}\" alt=\"No avatar\">"
    end
  end

end
