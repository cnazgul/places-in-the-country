class Map < ActiveRecord::Base
  attr_accessible :title, :category_id, :coord, :more, :image, :reach

  validates :title, :category_id, :coord, :more, :reach, presence: true
  validates :title, :length => {in: 6..30}
  validates :more, :reach, :length => {in: 40..120}
  validates :coord, format: { with: /\A[0-9,.]+\z/ }

  has_many :commentaries
  belongs_to :category
  belongs_to :user

  scope :moderation, ->(state) { where("moderation = ?", state) }
  mount_uploader :image, ImageUploader

  def self.unload_place(gon)
    gon.map_id = []
    gon.map_coord = []
    gon.map_title = []
    gon.map_img = []
    gon.str = []

    for num in (1..6)
      gon.map_coord[num-1] = [num]
      gon.map_img[num-1] = [num]
      gon.map_title[num-1] = [num]
      gon.map_id[num-1] = [num]
      i = 0
      j = 0
      Map.where('moderation = 0 and category_id = ?', num).each do |m|
        gon.str = m.coord.split(',')
        gon.map_coord[num-1][i] =  gon.str[0].to_f
        gon.map_coord[num-1][i+1] =  gon.str[1].to_f
        gon.map_title[num-1][j] = m.title
        gon.map_img[num-1][j] = m.image_url   ### img_url
        gon.map_id[num-1][j] = m.id
        i = i + 2
        j = j + 1
      end
    end
  end

end
