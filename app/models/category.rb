class Category < ActiveRecord::Base
  attr_accessible :title, :id

  validates :title, presence: true
  validates :title, :length => {in:4..12}

  has_many :maps

end
