class Commentary < ActiveRecord::Base
  attr_accessible :description, :map_id, :user_id

  validates :description, :user_id, presence: true
  validates :description, :length => {in:2..300}
#  validates :description, format: { with: /\A[а-яА-ЯёЁa-zA-Z0-9.,+-:_]+\z/}

  belongs_to :user
  belongs_to :map

  scope :moderation, ->(state) { where("moderation = ?", state) }
  scope :find_by_map_id, ->(id) { where("map_id = ?", id) }
end
